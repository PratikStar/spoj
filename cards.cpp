//
//  main.cpp
//  Indeed
//
//  Created by Sutar, Pratik on 3/13/19.
//  Copyright © 2019 Sutar, Pratik. All rights reserved.
//

#include <iostream>     // std::cout
#include <algorithm>    // std::sort
#include <vector>       // std::vector
#include <string>
#include <stdio.h>

using namespace std;


int main(int argc, const char * argv[]) {
    unsigned long long int t, a, b;
    cin >> t;
    while(t--) {
        cin >> a;
        b = (((3*a + 1)  * a)  / 2) % 1000007 ;
        cout << b << endl;
    }

}
